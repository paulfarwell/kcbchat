<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
$detector = new \Mobile_Detect();
$bodyclass = "";

if ($detector->isMobile()) {
    $bodyclass = "mobile";
}
header('Access-Control-Allow-Origin: http://kcbchat.farwell-consultants.com:433');
header('Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
header('Access-Control-Allow-Methods: GET, PUT, POST');
header('Access-Control-Allow-Credentials:true');

?>

<?php 
// $noconflict = <<<JS
//   $.fn.bsModal = $.fn.modal.noConflict();
// JS;

$jshideflash = <<<JS
setTimeout(function() {
    $('.alert').fadeOut('slow');
}, 4500); // <-- time in milliseconds
JS;

// $jsfootermodal = <<<JS
// $("#terms_conditions").on("click", function(event) {
//     event.preventDefault();
//     $("#theterms").modal("show");
// });
// JS;

// $this->registerJs($noconflict, \yii\web\View::POS_READY);
$this->registerJs($jshideflash, \yii\web\View::POS_READY);
// $this->registerJs($jsfootermodal, \yii\web\View::POS_READY);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <?= $this->registerJsFile('/js/socket.io-1.4.5.js', ['position' => \yii\web\View::POS_HEAD]); ?>
    <?= $this->registerJsFile('/js/to-top.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
    <?php $this->head() ?>
</head>
<body id="page-top" class="index <?php echo $bodyclass;?>  ">
<div class="wrapper">
<?= $this->render('@frontend/views/templates/_chat_banner') ?>


<?php $this->beginBody() ?>

<?= $content ?>
<?= $this->render('@frontend/views/templates/_terms-conditions') ?>
</div><!-- wrapper -->
<div class="push"></div>  

<footer class="footer-section">
    <div class="container">

        <div class="row">

            <div class="col-md-6 col-xs-12">
                <span class="copyright">
                    Copyright &copy; KCB Group Limited <p class="year"><?= date('Y') ?></p>
                </span>
            </div>
            <!-- <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div> -->
            <div class="col-md-6 col-xs-12">
                <a href="#" id="terms_conditions" data-toggle="modal" data-target="#theterms">Terms of Use</a>
                &nbsp;&nbsp; | &nbsp;&nbsp; 
                <a href="
                <?= Url::to('http://www.farwell-consultants.com');?>
                " class ='farwell_link' target = '_blank'> Developed by FCL</a>
                
                <!-- <a class="farwell_link" href="www.farwell-consultants.com" target="_blank">Developed by <strong>FCL</strong></a> -->
                <!-- <ul class="list-inline quicklinks">
                    <li><a href="#">Privacy Policy</a>
                    </li>
                    |
                    <li><a href="#" id="terms_conditions" data-toggle="modal" data-target="#theterms">Terms of Use</a>
                    </li>
                </ul> -->
            </div>
                
        </div><!-- main footer row -->

        <!-- <div class="row">
            <div class="col-md-6 col-xs-12">
                <a class="farwell_link" href="www.farwell-consultants.com" target="_blank">Developed by <strong>FCL</strong></a>
            </div>
        </div> -->

        
        <span class="back-to-top" style="display: inline;"></span>
    </div>
</footer>

<?php $this->endBody() ?>

 <script>
     $(document).ready(function(){
    $("#message-field").on("keyup",function(){
        var str = $(this).val();
        if(str.indexOf("@") == 0 && str.length > 1){
          var term = str.slice(1,30);
          console.log(term)
          $.ajax({
        type: "POST",
        url: "/site/autocomplete",
        data:'term='+ term,
        success: function(strData){
             var data = JSON.parse(strData);
             var options
            data.data.forEach(function(user){
             options += '<option value="' + user.user_id + '">' + user.firstname +' '+user.lastname+ '</option>';
             
             });
            $('select#mentions').show();
             $("select#mentions").html(options);
             // $("#suggesstion-box").show();
             // $("#suggesstion-box").html(data);
            // $("#message-field").css("background","#FFF");
        }
        });
        }else{

            console.log("not")
        }     
    });

$("#mentions").on("change",function(){
    var m = document.getElementById('mentions');
     document.getElementById('message-field').value = "";
     var f = m.options[m.selectedIndex].text; 
     var mention = m.options[m.selectedIndex].value;
    $('#message-field').val('@'+f);
    $('#mention-hidden').val(mention);

    });


$('.file-input').change(function() {
    $file = $(this).val();
    $file = $file.replace(/.*[\/\\]/, ''); //grab only the file name not the path
    $('.filename-container').append("<span  class='filename' id='file-holder'>" + $file + "</span>").show();
   
  });



});


//To select country name
 </script> 
</body>
</html>
<?php $this->endPage() ?>
