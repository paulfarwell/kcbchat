<?php
use yii\helpers\Html;
$settings = Yii::$app->settings;
$settings->clearCache();
$logo = $settings->get('Theme.logo');
$banner = $settings->get('Theme.banner');

?>
<nav class="navbar navbar-inverse ">

 <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
    <div class="navbar-header">
         <div class="navbar-brand">
                  <?php echo Html::a( Html::img('@web/images/site/'. $logo, ['class' => 'slider-image']), ['/chat']); ?>
          </div>

    </div>
  </div>
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
  <div id="slider" class="pull-right chat-banner">
 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="9000">

   <div class="carousel-inner" role="listbox">
     <div class="item active">
       <?php echo Html::a( Html::img('@web/banner/KCB Strategic Points Banner Design_01_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
     <div class="item">
     <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_02_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
      <div class="item">
     <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_03_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
      <div class="item">
     <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_04_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
      <div class="item">
     <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_05_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
      <div class="item">
     <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_06_v3.png', ['class' => 'slider-image']), ['/chat']); ?>
     </div>
   </div>

 </div>
 </div>
</div>
</nav>
