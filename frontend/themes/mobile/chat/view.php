<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;

use yii\widgets\Breadcrumbs;

$this->title = $model->Topic;;
$this->params['breadcrumbs'][] = ['label' => 'Chat Archives', 'url' => ['chat/archives']];


?>
<?php 
$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$popup = <<<JS
  $('.teal.button')
  .popup({
    on: 'click'
  })
;
JS;
$transcripts = <<<JS


jQuery("#transcriptsmodal").modal();

jQuery(".load_trans_btn").on("click", function(event) {
    event.preventDefault();
    jQuery("#transcriptsmodal").modal("show");
});
JS;

$this->registerJs($noconflict, \yii\web\View::POS_READY);
$this->registerJs($popup, \yii\web\View::POS_READY);
$this->registerJs($transcripts, \yii\web\View::POS_READY);

$this->registerJsFile('/js/tableHeadFixer.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$jass = <<<JS
 $("#fixTable").tableHeadFixer();
JS;
$this->registerJs($jass, \yii\web\View::POS_READY);

$this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$jsspec = <<<JS
$('.match_height').matchHeight();
JS;
$this->registerJs($jsspec, \yii\web\View::POS_READY);

?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="chat_trascript_page chat_details_page">
  <div class="row" id="podcasts_section">
    
    <span class="page-title">
    
      <span class="col-xs-12 col-sm-10"><h1 class="page-header"><?php echo $model->Topic; ?></h1></span>
      <span class="loginout col-xs-12 col-sm-2">
        <?php if (Yii::$app->user->isGuest):?>
          <a class="btn-login" href="<?= Url::to(['site/register', '#' => 'login'])?>"> Log In </a>
          <?php else:?>
          <a class="btn-login" href="<?= Url::to(['user/security/logout'])?>"  data-method="post"> Log Out  </a>
        <?php endif; ?>

      </span><!--  loginout -->

    </span> <!-- page-title -->

    <div class="container">

        <div class="row">

            <div class="col-md-6 match_height">

                <div class="row breath">
                    <?= Breadcrumbs::widget([
                         'homeLink' => [ 
                                  'label' => Yii::t('yii', 'Home'),
                                  'url' => Yii::$app->homeUrl,
                             ],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    
                    <div class="load_trans">
                        <span class="load_trans_btn"> LOAD CHAT TRANSCRIPT </span>
                    </div>

                </div>

                <?php if ($model->Podcast): ?>
                <div class="row">
                    
                        <div class="embed-responsive embed-responsive-4by3 podcast">
                            <iframe src="//www.youtube.com/embed/<?php echo $model->Podcast?>" class="embed-responsive-item" allowfullscreen></iframe>
                        </div>
                </div>

                <?php endif; ?> 
            </div>


            <div class="col-md-6  ">
                
                <div class="match_height container-grey chat_view_sec">
                      
                    <div class="holder">
                                
                        <div class="row">

                            <div class="media main-chat-btns">
                              <div class="media-left">
                                <?= Elements::image($model->host->profile->getImgSrc('avatar'), ['class' => 'ui img-circle image media-object']) ?>
                              </div>
                              <div class="media-body">
                                <span class="bordered-btns-container">
                                    <?php $ceomodal = Modal::begin([
                                        'size' => Size::SMALL,
                                        'header' => 'CEO\'s Profile',
                                        'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
                                    ]); ?>
                                      <?= $this->render('@frontend/views/user/profile/profilepopup', ['model'=>$model->host])?>

                                    <?php $ceomodal::end(); ?>
                                    <?= $ceomodal->renderToggleButton('Ceo\'s Profile',['class' => 'ceo_launcher btn-bordered btn-grey btn-default']) ?>

                                    <?php $infomodal = Modal::begin([
                                        'size' => Size::SMALL,
                                        'header' => 'Topic Areas',
                                        'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
                                    ]); ?>
                                      <?= $this->render('@frontend/views/chat/topic_info_popup', ['model'=>$model])?>

                                    <?php $infomodal::end(); ?>
                                    <?= $infomodal->renderToggleButton('Topic Areas',['class' => 'info_launcher btn-bordered btn-orange btn-default']) ?>

                                </span>
                              </div>

                            </div><!-- media main-chat-btns -->

                        </div><!-- row -->
                        
                        
                        <div class="row">
                            <hr>

                            <ul class=" list-group no-border">
                                <li class=" list-group-item">
                                    <span>CEO : </span>
                                    <span class="_rigth first"><?php echo $model->Ceostring ?></span>
                                </li>
                                <li class=" list-group-item topic">
                                    <span>MONTH : </span>
                                    <span class=""><?php echo Html::encode(Yii::$app->formatter->asDate($model->ChatDate,'MMMM')." ". date('Y', strtotime($model -> ChatDate))); ?></span>
                                </li>
                                <li class=" list-group-item topic">
                                    <span>THEME : </span>
                                    <span class=""><?php echo $model->edition->Theme ?></span>
                                </li>
                                <li class=" list-group-item topic">
                                    <span>TOPIC : </span>
                                    <span class=""><?php echo $model->Topic ?></span>
                                </li>
                                
                            </ul>

                             <hr>
                         </div><!-- row -->

                    </div> <!-- holder -->

                </div><!-- chat_view_sec -->

             </div><!-- chat_info -->
    
        </div><!-- row -->
        
        <div id="transcriptsmodal" class="ui long modal">

            <div class="chat_transcript  panel panel-default scrollable" id="the_chat_transcript">
              
                <div class="panel-heading">CHAT TRANSCRIPT 
                 <?php if ($model->Log): ?>
                    <?= Html::a('Download', Url::toRoute(['chat/download', 'id' => $model->ID]), ['title' => $model->Topic . "Details", 'class'=>"download_link btn btn-primary btn-xs"]) ?>
                <?php endif; ?>
                </div> <!-- panel-heading -->

                <div class="table-responsive" id="table_parent">

                 <table class="table table-bordered table-striped table-hover" id="fixTable">
                 <thead>
                     <tr>
                         <th>Timestamp</th>
                         <th>Source</th>
                         <th>Message</th>
                     </tr>
                 </thead>
                 <tbody>
                     <?= $this->render('@frontend/views/chat/transcript', ['messages'=>$messages])?>
                </tbody>
                </table>

                </div><!-- table-responsive -->

            </div><!-- panel-default chat_transcript  -->

            <div class="actions"><div class="ui cancel right labeled icon button">Exit<i class="remove icon"></i></div></div>

            </div><!-- row -->

        </div><!--  container -->

    </div><!--  events_section -->
</div> <!-- row -->
