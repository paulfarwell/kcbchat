$(function(){
    $('#host-notificationsBody').slimScroll({
        height: '250px'
    });
});
$("#host-notificationLink").click(function()
{
	$("#host-notificationContainer").fadeToggle(300);
	$("#host-notificationsBody" ).last().addClass( "host-notification_seen" );
	$("#host-notification_count").fadeOut("slow");
	return false;
});
//Document Click hiding the popup 
$(document).click(function()
{
$("#host-notificationContainer").hide();
$("div.host-notification_seen > div.host-notification_single").remove();
});

//Popup on click
$("#host-notificationContainer").click(function()
{
return false;
});

