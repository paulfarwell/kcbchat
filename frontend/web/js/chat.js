// var socket = io.connect('https://testchat.kcbgroup.com:8080',{reconnection: false});
var socket = io.connect('http://127.0.0.1:8000',{reconnection: false});

socket.emit("new_conn", {
  mydata: {msg: "New connection established!"}
});

socket.on('notification', function (data) {

    var message = JSON.parse(data);

    $( "#notifications" ).prepend( "<p><strong>" + message.name + "</strong>: " + message.message + "</p>" );

});
