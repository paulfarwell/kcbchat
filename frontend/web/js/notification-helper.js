$(function(){
    $('#notificationsBody').slimScroll({
        height: '250px'
    });
     $('#topicsBody').slimScroll({
        height: '250px'
    });
});
$("#notificationLink").click(function()
{
	$("#notificationContainer").fadeToggle(300);
	$( "#notificationsBody" ).last().addClass( "notification_seen" );
	$("#notification_count").fadeOut("slow");
	return false;
});
//Document Click hiding the popup 
$(document).click(function()
{
$("#notificationContainer").hide();
$("div.notification_seen > div.notification_single").remove();
});

//Popup on click
$("#notificationContainer").click(function()
{
return false;
});

