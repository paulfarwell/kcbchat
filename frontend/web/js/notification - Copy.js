
$( document ).ready(function() {

    //var socket = io.connect('http://localhost:8890/socket',{ query: 'roomdata='+chatRoom.id+'' });
    console.log("LOGGING VAR chatRoom === ",chatRoom, " === >");
   
   var activeChatArea = $("#area_active_chat");
   var chatForm = $("#chat-form");
   var messageval = $("#message-field");
   var onlineUsersArea=$("#user_feed");
   var activeUsersArea=$("#user_feed_active");
   var onlineUsersStats=$("#user_stats");
       messageval.keypress(function(e){

            if(e.which == 13) {
                e.preventDefault();
                chatForm.trigger('submit');
            }

        });
    chatForm.on('submit', function(e){

        e.preventDefault();
        var msgentered= messageval.val();
        
        var textmsgcontent = msgentered.trim();
        if(textmsgcontent.length == 0){
            return false;
        }
        // Create a new chat message and display it directly

        //showMessage("chatStarted");

        //createChatMessage(textarea.val(), 'local',name, img, moment());
        //scrollToBottom();
        scrollToChatBottom();

        // Send the message to the other person in the chat
        socket.emit('newmessage', {msg: messageval.val(), user: chatRoom.chatUserName,userID: chatRoom.chatUserId,userRole: chatRoom.role});

        // Empty the textarea
        messageval.val("");
    });

    var oldMsgButton=$("#loadOldMsgs");
    var channel = 'notification';    
    console.log("LOGGING VAR channel === ",channel, " === >");

    socket.on('connect', function(){
        socket.emit('room', {room: chatRoom.id, maxUsers: chatRoom.maxUsers, userID: chatRoom.chatUserId});
        socket.emit('login', {key: chatRoom.chatKey, id: chatRoom.id, maxUsers: chatRoom.maxUsers, avatarpath: chatRoom.avatarpath});
    }); 

    socket.on(channel, function (data) {

        var message = JSON.parse(data);
         createChatMessage (message.id, message.message, message.chatUserName, message.avatar, moment(), message.role);
    });
    socket.on('receive', function(data){
        console.log("LOGGING socket.on('receive' === ",data, " === >");
        //createChatMessage (message.id, message.message, message.chatUserName, message.avatar, moment(), message.role);
        createChatMessage( data.msgid , data.msg, data.user, data.avatar , moment(),data.userRole);         
            // //scrollToBottom();
            scrollToChatBottom();
            
            // notifyMessage(data.user);
    });

    socket.on('roommates',function(data){
        data.forEach(function(user){
            console.log("LOGGING socket.on('roommates' === ",user, " === >");
                createNewUser(user.userid, user.Name , user.avatar, user.activeStatus );
        });
    });

    socket.on('newuser',function(data){
        console.log("LOGGING socket.on('roommates' === ",data, " === >");
        createNewUser(data.userid, data.Name ,data.avatar, data.activeStatus);
    });

    socket.on('onlineusers',function(data){
        //console.log(data);
        var onlineusers = data + " USERS ONLINE";
        onlineUsersStats.html(onlineusers);
        //createNewUser(data.userid, data.Name ,data.avatar);
    });
    

    socket.on('likemsgid',function(data){
        msgliked(data);
        //console.log(data);

    });

    oldMsgButton.click(function(e){
        socket.emit('receiveOldMsg', {chatRoom});
    });

    socket.on('oldMsgs',function(data){
        var msgdata=data.theMsgs;
        console.log('LOGGING socket.on(oldMsgs ', msgdata, '  =====');
        msgdata.forEach(function(msg){
            createChatMessage (msg.ID, msg.MessageText, msg.UserName, chatRoom.avatar, moment(msg.ChatTime), chatRoom.role, msg.likes, msg.role);
        });
        oldMsgButton.hide();
        scrollToChatBottom();
        
    });

    function createChatMessage(msgid, msg , user, img, now, userRole, likes, role){
        var chattime = now.format("h:mm:ss a");
        var userClass=role;
        likes = typeof likes !== 'undefined' ? likes : 0;
        // role = typeof role !== 'undefined' ? role : 1;

        if(user==chatRoom.chatUserName){
            userClass='me';
        }


        var adminview = $(
                    '<div class="per-chat '+ userRole.toLowerCase() + ' ' + userClass +'" id="active_msg_'+ msgid +'">'+
                        '<div class="col-xs-12 col-sm-3 col-md-2 user-name">'+
                            '<span class="msg_originator">'+ user + ' </span> ' +                           
                            '</div>'+
                        '<div class="col-xs-12 col-sm-9 col-md-10 posted-chat message_content">'+
                            '<span  class="conversation col-sm-10 col-xs-12"><span class="conversation_msg">' + youtube(msg) + '</span><span class="msg_time">' + chattime + ' </span>'+ '</span>'+
                        
                                '<span class="col-sm-2 posted-chat-actions hidden-xs msgtions">'+
                                    '<input type="checkbox" data-toggle="tooltip" data-placement="right" title="Filter Message" class="filter_msg axn" onclick="filterThisMsg('+ msgid +');"  />'+
                                    '<button type="button" data-toggle="tooltip" data-placement="right" title="Qoute Message" class="qoute_msg glyphicon glyphicon-copy axn" onclick="qouteThisMsg('+ msgid +');"  />'+
                                '</span>'+
                        '</div>'+
                    '</div>'
            );
        var userview = $(
                    '<div class="per-chat '+ userRole.toLowerCase() + ' ' + userClass + '" id="active_msg_'+ msgid +'">'+
                        '<div class="col-xs-12 col-sm-3 col-md-2 user-name">'+
                            '<span class="msg_originator">'+ user + ' </span> ' +                           
                            '</div>'+
                        '<div class="col-xs-12 col-sm-9 col-md-10 posted-chat message_content">'+
                            '<span class="conversation col-sm-10 col-xs-12"><span class="conversation_msg">' + '<span class="msg_content">'+ youtube(msg) + '</span></span><span class="msg_time">' + chattime + ' </span>'+ '</span>'+
                        
                                '<span class="col-sm-2 posted-chat-actions hidden-xs msg_actions">'+
                                    '<button type="button" data-toggle="tooltip" data-placement="right" title="Tweet Message" class="tweet_msg axn" onclick="tweetThisMsg('+ msgid +');"  /></button>'+
                                    '<div class="likeclass">'+
                                        '<button type="button" data-toggle="tooltip" data-placement="right" title="Like Message" class="like_msg glyphicon glyphicon-thumbs-up axn" onclick="likeThisMsg('+ msgid +');"  /></button>'+  
                                        '<span class="likecounter" id="msg_like_'+msgid+'">'+likes+'</span>'+
                                    '</div>'+
                                '</span>'+
                        
                        '</div>'+
                    '</div>'
            );
        console.log("== LOGGING createChatMessage() {chatRoom.role} ====",chatRoom.role, "===>");
        if(chatRoom.role!='USER'){  
            activeChatArea.append(adminview);
        }
        else{
            activeChatArea.append(userview);
        }

        
    }

    function likeThisMsg(msgid){
        socket.emit('likemsg', {msgid: msgid});
        //console.log("liking msg");
    }

    function createNewUser(userid ,name , avatar, activestatus ){
        console.log("=== LOGGING activestatus: ", activestatus, "======/");
        var liID ='user_'+userid;
        var user = $('<tr class="activeuser online_user '+ userid + '" id="user_'+ userid +'" data-content="user_'+ name +'">'+
                        '<td class="user_image online-user-avatar">'+
                            '<img class="img-circle" src="'+avatar+'" alt="'+name+'">'+
                        '</td>'+
                        '<td class="user_name online-user-info">'+
                            '<span class="avatar-bio">' + name + '</span>'+
                        '</td>'+
                    '</tr>'
        );
        if (activestatus == true ) {
            var liaID ='usera_'+userid;
            var usera = $(
                '<tr class="online_user '+ userid + '" id="usera_'+ userid +'" data-content="user_'+ name +'">'+
                    '<td class="user_image online-user-avatar">'+
                        '<img class="img-circle" src="'+avatar+'" alt="'+name+'">'+
                    '</td>'+
                    '<td class="user_name online-user-info">'+
                        '<span class="avatar-bio">' + name + '</span>'+
                    '</td>'+
                    '</tr>'+
                '</tr>'
                );
            //console.log($("#" + liaID).length);
            if($("#" + liaID).length == 0) {

                activeUsersArea.prepend(usera);
            }   
        }
        if($("#" + liID).length == 0) {
            onlineUsersArea.prepend(user);
         }
         
    }
    // $('#user_feed').on('mouseover mouseout', '.online_user', function() {
    //  var classname = $(this).attr('class');
    //  var res = "." + classname.replace(/\s/g, ".");
    //  $(res).popup('show',{
    //      popup:res
    //  });

    //  // console.log(this);
    //  console.log('the res  ', res);
    // });
    
/*
    function createNewUser(user){
        console.log(user);
        var liID='user_'+user.userid;
            
        var li=$(
            '<li class="activeuser '+user.userid +'" id="user_'+user.userid +'">'+
            '<p class="online_users_chatting">'+ user.Name +'</p>'+
            '<p class="clear"></p>'+
            '<li>'
            
        );
        if(user.activeStatus==1){
            var liaID='usera_'+user.userid;
            var lia=$(
                '<li class="onlineuser '+user.userid +'" id="usera_'+user.userid +'">'+
                '<p class="online_users_chatting">'+ user.Name +'</p>'+
                '<p class="clear"></p>'+
                '<li>'              
            );  
            if($("#" + liaID).length == 0) {
                //activeUsersArea
             activeUsersArea.prepend(lia);
            }                   
        }
        if($("#" + liID).length == 0) {
          onlineUsersArea.prepend(li);
        }
    }
    */

});

function scrollToChatBottom() {
    $(".chat-body").animate({ scrollTop: $(".chat-body")[0].scrollHeight}, 1000);
};

function filterThisMsg(msgid) {
    if(chatRoom.role!='USER'){
        var theLi='active_msg_'+msgid;
        var liTofilter=$("#"+theLi);
        var newLiInFilter;
        var newLiInFilterID='filtered_msg_'+msgid;
        newLiInFilter=$(liTofilter).clone().prop({ id: newLiInFilterID});
        $('#area_filtered_chat').append(newLiInFilter);
        liTofilter.hide();
        //get the ccheckbok
        $("#"+newLiInFilterID+" .filter_msg").prop('checked', false);
        $("#"+newLiInFilterID+" .filter_msg").attr("onclick","unfilterThisMsg("+msgid+")");
    }
}
function unfilterThisMsg(msgid){
    var theLi='filtered_msg_'+msgid;
    var liToUnfilter=$("#"+theLi);
    liToUnfilter.remove();
    var liToactivate=$('#active_msg_'+msgid);
    liToactivate.show();
    $("#active_msg_"+msgid+" .filter_msg").prop('checked', false);
}
function qouteThisMsg(msgid){
    var mytext =$('#active_msg_'+msgid+' .conversation_msg').text();
    console.log(mytext);
    var editedtext=' ..."' +mytext+'"';
    $('#message-field').val(editedtext);
    //console.log(mytext);
}
function youtube($string)
{
    
        return $string.replace(
          /(?:https:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g, 
          '<iframe width="83%" height="293" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>')
          .replace(
            /(?:http:\/\/)?(?:www\.)?(?:vimeo\.com)\/(.+)/g, 
            '<iframe src="//player.vimeo.com/video/$1" width="200" height="100" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'
            );

}



function msgliked(data){
    console.log('msgliked');
    var msgspanselector= 'span#msg_like_'+data.likedmsgid;
    $(msgspanselector).text(data.likedmsgcount);
    //console.log(data);

}
function tweetThisMsg(msgid){
    var mytext =$('#active_msg_'+msgid+' span.msg_content').text();
    var originator=$('#active_msg_'+msgid+' span.msg_originator').text();
    var qoutedmsg='"'+ mytext+'"';
    //console.log(qoutedmsg);
     var loc = $(this).attr('href');
    // var config='via=KenyaBankers& url=chat.kba.co.ke';
     window.open('http://twitter.com/share?via=KenyaBankers& url=' + loc + '&text=' + qoutedmsg + '&', 'twitterwindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    
}
function likeThisMsg(msgid){
    socket.emit('likemsg', {msgid: msgid});
    //console.log("liking msg");
}
