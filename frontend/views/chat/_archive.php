<?php
// _chats_view.php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Chat;
use Zelenin\yii\SemanticUI\Elements;
use frontend\models\User;
use backend\models\Host;
?>

<div class="archived_chat col-lg-4 col-md-4 col-sm-6 col-xs-12">
	<div class="archive_chat_holder">
		 <?php if (Host::findIdentity($model['chatHost'])->profile->avatar):?>
			 <div class = "image_holder">
				<!-- @web/images/avatar/'.User::findIdentity($model['Host'])->profile->avatar, -->
				<?= Html::img('@web/images/avatar/'.Host::findIdentity($model['chatHost'])->profile->avatar, ['class' => 'img-circle img-responsive','style'=>'width:220px;height:200px']) ?> 
				<!-- archive_image -->
			</div>
		<?php else:?>
              <div class = "image_holder">
				<?= Html::img('@web/images/avatar/user.jpg',['class' => 'img-circle img-responsive','style'=>'width:220px;height:200px'])?>
			</div>
		<?php endif;?>
		<div class="topicinfo">

			<span style="display:inline-flex;"><p><b style="color:#064367;">Host: </b></p>&nbsp; &nbsp;&nbsp; <p>  <?php echo Host::findIdentity($model['chatHost'])->profile->name .' '.Host::findIdentity($model['chatHost'])->profile->lastname?></p></span><br>
			<span style="display:inline-flex;"><p><b style="color:#064367;">Title: </b></p>&nbsp; &nbsp;&nbsp; <p>  <?php echo $model['Topic']; ?></p></span><br>
			<span style="display:inline-flex;"><p><b style="color:#064367;">Date :  </b></p>&nbsp;&nbsp;&nbsp; <p><?php echo date("jS F, Y",strtotime($model['ChatDate'])); ?></p>
		</div>
		<div class="details_sect">
			<?php if ($model['Podcast'] != NULL):?>
				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Podcast', Url::toRoute(['chat/view', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-archive '] ) ?>
			<?php endif;?>
			
				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Transcript', Url::toRoute(['chat/view', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-archive ']) ?>
			
				<?= Html::a(Html::tag('span', '', ['class' => 'archive_links', 'aria-hidden' => 'true',]).' Details', Url::toRoute(['chat/details', 'id' => $model['ID']]), ['title' => $model['Topic'] . " Read", 'class' => 'btn btn-archive ']) ?>
		</div>
		
	</div><hr><!-- 	archive_chat_holder -->
</div><!-- MAIN col-xs-12 col-md-6 -->
