<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/kcb.png" type="image/x-icon" />
<link rel="stylesheet" href="base.min.css"/>
<link rel="stylesheet" href="fancy.min.css"/>
<link rel="stylesheet" href="main.css"/>
<script src="compatibility.min.js"></script>
<script src="theViewer.min.js"></script>
<script>
try{
theViewer.defaultViewer = new theViewer.Viewer({});
}catch(e){}
</script>
<title></title>
</head>
<body>
<div id="sidebar">
<div id="outline">
</div>
</div>
<div id="page-container">
   <table align="center">
   	<tr><td><img class="" alt="" src="/images/site/logo.png" /></td></tr>
   </table>

  <h1 align="center">Kcb Group Chat</h1>
  <hr> 

  <h4>Host: <?php echo $model->Ceostring ?></h4>
  <h4>Topic: <?php echo $model->Topic ?></h4>
  <h4>Date: <?php echo Html::encode(Yii::$app->formatter->asDate($model->ChatDate,'MMMM')." ". date('Y', strtotime($model->ChatDate))); ?></h4>

<div style="margin-top:20px">
<?php foreach($messages as $message):?>
	<div style="margin-top:5px">
	<span >
		<?php if($message->role == 1):?>
			<label><span style="color:#52c4ce;"><b><?php echo $message->UserName;?>:</b></span></label>
             <?php elseif($message->role == 2):?>
             <label ><span style="color:#95C13D;"><b><?php echo $message->UserName;?>:</b></span></label>
         <?php else:?> 
         	<label ><span style="color:#002d3f;"><b><?php echo $message->UserName;?>:</b></span></label>
         <?php endif;?>	</span> 
	<span style="padding-left: 10px;"><?php echo $message->MessageText;?></span>
   </div>
<?php endforeach;?>
</div>
</div>
</body>
</html>