<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\helpers\Size;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chat Archives';
$this->params['breadcrumbs'][] = $this->title;

/* equal heights */
// $this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
// $js = <<<JS
//  $('.archive_chat_holder').matchHeight();
// JS;
// $this->registerJs($js, \yii\web\View::POS_READY);

$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$this->registerJs($noconflict, \yii\web\View::POS_READY);
?>

<div class="chat-index row">
    <?= $this->render('_archivemenu',['searchModel'=>$searchModel]) ?>
       
        
      <!--   <span class="search_btn hidden-md hidden-lg" >

        <?php $searchmodal = Modal::begin([
            'size' => Size::LARGE,
            'header' => 'Filter Archives',
            'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
        ]); ?>
          <?= $this->render('_archivesearch', ['model' => $searchModel]);?>

        <?php $searchmodal::end(); ?>
        <?= $searchmodal->renderToggleButton(Elements::icon('search')) ?>

        </span>
        
        <div class="hidden-sm hidden-xs">
                <?php echo $this->render('_archivesearch', ['model' => $searchModel]); ?>
        </div>
 -->
 <div class="chat-background-over">
 <div class="chat_archives_back">
    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [ 
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <div class="row archived_chat_container">
        <?php 
            echo ListView::widget([
                 'dataProvider' => $dataProvider,
                 'layout' => "{items}\n{pager}\n",
                 'itemOptions' => ['class' => 'item'],
                 'itemView' => '_archive',
                 // 'itemOptions' => [
                 //     'tag' => false,
                 // ],
                 'pager' => [
                    'class' => \kop\y2sp\ScrollPager::className(),
                    'container' => '.archived_chat_container',
                    'item' => '.archived_chat',

                 ]
            ]);

        ?>
        
        </div>
    </div><!-- container -->
</div><!-- chat-index -->
</div>
</div>