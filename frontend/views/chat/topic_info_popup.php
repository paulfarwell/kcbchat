<?php
/**
 * var $model  chat model
 * 
 */
 //print_r($model);
use yii\helpers\Html;

?>

<div class="chat_info list-group">
		<ul>
			<li class=" list-group-item active">
				<span class="chat_info_left" >Chat Topic: </span>
				<span class="chat_info_rigth first"><?php echo $model->Topic ?></span>
			</li>
			<li class="list-group-item">
				<span class="chat_info_left">Time: </span>
				<span class="chat_info_rigth"><?php echo Html::encode(Yii::$app->formatter->asDatetime($model->StartTime,'h:mm a')." - ".Yii::$app->formatter->asDatetime($model->EndTime,'h:mm a')); ?></span>
			</li>
			<li class="list-group-item">
				<span class="chat_info_left">Date: </span>
				<span class="chat_info_rigth"><?php echo Html::encode($model->ChatDate); ?></span>
			</li>
			<li class="list-group-item">
				<span class="chat_info_left">Host: </span>
				<span class="chat_info_rigth"><?php echo Html::encode($model->host->Ceostring); ?></span>
			</li>
		</ul>
 </div>