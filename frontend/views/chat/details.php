<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
use yii\widgets\Breadcrumbs;
use dosamigos\gallery\Gallery;
?>
<?php
$this->title = 'Chat Details';
$this->params['breadcrumbs'][] = ['label' => 'Chat Archives', 'url' => ['chat/archives']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$this->registerJs($noconflict, \yii\web\View::POS_READY);
?>
 <?= $this->render('@frontend/views/templates/metro_register') ?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="chat_details_page the_chat_details_page spacer-sm row">

    <div id="spec_chat_details">

        <span class="page-title">

          <span class="col-xs-12 col-sm-10"><h1 class="page-header"><?php echo $model->Topic; ?></h1></span>

         <!--  loginout -->
        </span> <!-- page-title -->

    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>



        <div class="chat_info list-group col-md-12 col-xs-12 col-lg-12 col-sm-12">
            <ul>
                <li class=" list-group-item active theme">
                    <span class="chat_info_left" >TOPIC: </span>
                    <span class="chat_info_rigth first"><?php echo ucfirst($model->Topic); ?></span>
                </li>
                <li class=" list-group-item topic">
                    <span class="chat_info_left" >Host: </span>
                    <span class="chat_info_rigth first"><?php  if($model->host->profile !== null){echo $model->host->profile->name ?> <?php echo $model->host->profile->lastname;}else{echo $model->hoststring;} ?></span>
                    <span class="chat_info_left" >
                            <?php $ceomodal = Modal::begin([
                          'size' => Size::SMALL,
                          'header' => 'CEO\'s Profile',
                          'actions' => Elements::button('Close' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
                      ]); ?>
                    <?= $this->render('@frontend/views/user/profile/profilepopup', ['model'=>$model->host])?>

                    <?php $ceomodal::end(); ?>

                    <?= $ceomodal->renderToggleButton('View Host\'s Profile',['class' => '']) ?>
                    </span>
                </li>
                <li class="list-group-item month">
                    <span class="chat_info_left">MONTH: </span>
                    <span class="chat_info_rigth"><?php echo Html::encode(Yii::$app->formatter->asDate($model->ChatDate,'MMMM')." ". date('Y', strtotime($model -> ChatDate))); ?></span>
                </li>
                <li class="list-group-item fulldetails">
                    <span class="chat_info_left">Description: </span>
                    <span class="chat_info_rigth"><?php echo $model->Description ?></span>
                </li>

            </ul>
         </div><!-- chat_info -->
         <div class=" chat_info list-group col-md-12 col-xs-12 col-lg-12 col-sm-12">

           <?php if (!empty($model->gallery)):?>
              <h5> Chat Gallery</h5>
         <?php $items;?>

          <?php foreach($model->gallery as $gallery):?>


        <?php $items = [
    [
        'src' => '@web/chatGalleries/'.$gallery->image1,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image2,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image3,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image4,
        'options' => array('title' => $model->Topic)
    ],


    ];?>
  <?php $item = [
    [
      'src' => '@web/chatGalleries/'.$gallery->image5,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image6,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image7,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image8,
        'options' => array('title' => $model->Topic)
    ],

  ];?>
  <?php $ite = [
    [
      'src' => '@web/chatGalleries/'.$gallery->image9,
        'options' => array('title' => $model->Topic)
    ],
    [
      'src' => '@web/chatGalleries/'.$gallery->image10,
        'options' => array('title' => $model->Topic)
    ],

  ];?>
      <?php endforeach;?>
<?= Gallery::widget(['items' => $items]);?>
<?= Gallery::widget(['items' => $item]);?>
<?= Gallery::widget(['items' => $ite]);?>
<?php endif;?>
</div>
      </div><!-- container -->

    </div><!--  events_section -->
</div> <!-- row -->
