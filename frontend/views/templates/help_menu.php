<?php 
use yii\helpers\Url;
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">

       <ul class="nav navbar-nav "><li><h2 style="color:#ffffff"><i class="fa fa-question fa-1x" style="color:#95C13D"></i><b>Help</b></h2></li>
         <li><p style="color:#fff; padding:8px">Here are some easy steps to get you started and make</br> the most out of the KCB Group Chat Platform</p></li>
       </ul>
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
      
       <li><a href="<?= Url::toRoute(['/',])?>" ><span class="fa fa-home"></span>Home</a></li>
  <li><a href="<?= Url::toRoute(['chat/enter'])?>"><span class="fa fa-comment-o"></span>Enter Chat Room</a> </li>
    <?php if (Yii::$app->user->isGuest):?>
    <li ><a href="<?= Url::toRoute(['site/login'])?>" ><span class="fa fa-lock "></span>Login</a></li>
     
  <?php else:?>
    <li ><a href="<?= Url::to(['/chat/archives'])?>" ><span class="fa fa-archive " data-method="post"></span>Chat Archive</a></li>
   <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-user"></span>MY PROFILE<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?= Url::to(['/user/settings/myprofile'])?>"><span class="fa fa-user"></span>My Profile</a></li>
                  <li ><a href="<?= Url::to(['/site/logout'])?>" data-method="post" ><span class="fa fa-lock " ></span>Logout</a></li>
                </ul>
              </li>
  <?php endif;?>
   <li><a href="<?= Url::to(['/site/help'])?>"><span class="fa fa-question"></span>Help</a></li>
    
      </ul>
    </div>
  </div>
</nav>