<?php 
use yii\helpers\Url;
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <div class="container">
      <div class="navbar-header">
        <h1 class="nav-brand" style="text-transform:initial;color:#95C13D">My Profile</h1>
      
    </div>
      <!-- <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li> 
        <li><a href="#">Page 3</a></li> 
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
        <li style=""><h2><a href="<?= Url::to(['/site/logout'])?>" data-method="post" style="font-size:16px; border: 1px solid #ffffff; border-radius:25px; padding: 5px; text-transform: initial; font-weight:bold"><span class="fa fa-chevron-left " style="padding-right: 5px" ></span>Logout</a><h2></li>
      </ul>
    </div>
    </div>
  </div>
</nav>