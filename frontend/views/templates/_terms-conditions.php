<div id="theterms" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="theterms">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="modal-title" id="myModalLabel">TERMS AND CONDITIONS</h2>
        </div>
        <div class="modal-body">
                    <p>
                      If you utilize the CEO Chat Registration Form you are agreeing to comply with
                      and be bound by the following terms and conditions of use, which together
                      with our privacy policy govern Kenya Bankers Association relationship with
                      you in relation to KBA website(s).
                    </p>
                    <p>
                      The term &ldquo;Kenya Bankers Association&rdquo;, &ldquo;KBA&rdquo; or &ldquo;us&rdquo; or &ldquo;we&rdquo; refers to the
                      owner of the website whose registered office is P.O. Box 73100 - 00200, Nairobi,
                      Kenya. Our Association is registered in the Republic of Kenya. The term &ldquo;you&rdquo;
                      refers to the user or viewer of our website.
                    </p>
                    <p>
                      The use of this website is subject to the following terms of use:
                      </p>

                      <ul>
                        <li>
                          The content of the pages of this website is for your general information and use only. It is subject to change without notice.
                        </li>
                        <li>
                          Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of
                          the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude
                          liability for any such inaccuracies or errors to the fullest extent permitted by law.
                        </li>
                        <li>
                          Your use of any information or materials on this website, including online conversations or chat sessions or visual, video and audio
                          presentations, is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products,
                          services or information available through this website meets your specific requirements.
                        </li>
                        <li>
                          Participants of online forums hosted by and endorsed by KBA must be
                          a minimum age of 18 years. By acknowledging this age requirement
                          via registration form you certify that you are at least 18 years old.
                        </li>
                        <li>
                          This website may contain material which is owned by or licensed to
                          KBA. This material includes, but is not limited to, the design, layout, look,
                          appearance and graphics. Reproduction is prohibited other than in
                          accordance with the copyright notice, which forms part of these terms
                          and conditions.
                        </li>
                        <li>
                          All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.
                        </li>
                        <li>
                          Unauthorized use of this website may give to a claim for damages and/or be a criminal offence.
                        </li>
                        <li>
                          From time to time this website may also include links to other websites.These links are provided for your convenience to provide further
                          information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
                        </li>
                        <li>
                          You may not create a link to this website from another website or document without Kenya Bankers Association’s prior written consent.
                        </li>
                        <li>
                          Your use of this website and any dispute arising out of such use of the website is subject to the laws of Kenya.
                        </li>
                      </ul>
                    <p></p>
                    <hr>
                    <h2>Copyright and Trademarks</h2>
                    <p>
                      &ldquo;my chat with a bank CEO&rdquo;, &ldquo;my CEO Chat&rdquo;, &ldquo;my 15min chat with a bank CEO&rdquo;, &ldquo;If you had 15mins with a bank CEO, what would you say or ask?&rdquo;, &ldquo;If
                    you had 15mins with a bank CEO, what would you talk about?&rdquo;, &ldquo;my 15min chat with a bank CEO&rdquo;, &ldquo;my bank CEO chat&rdquo;, &ldquo;myCEO chat&rdquo; are all registered trademarks of the Kenya Bankers Association.
                    </p>
                    <p>
                      Individual contributions (including questions and statements) submitted via KBA web site, online registration forms or during online discussions, chat events and forums will become the Copyright of the Kenya Bankers Association.
                    </p>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Exit</button>
        </div>

      </div>
    </div>



  </div>