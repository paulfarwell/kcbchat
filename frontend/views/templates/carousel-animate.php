<div class="carousel-section">
  <!-- <h3 class="carousel-section-head">The Events Include</h3> -->

  <div id="carousel-example-captions" class="carousel slide carousel-container" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-captions" data-slide-to="0" class=""></li>
      <li data-target="#carousel-example-captions" data-slide-to="1" class=""></li>
      <li data-target="#carousel-example-captions" data-slide-to="2" class="active"></li>
    </ol>

    <div class="carousel-inner carousel-body">
      <div class="item">
        <div class="carousel-caption">
          <h3 class="carousel-section-head">The Events Include</h3>
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>
          
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>

        </div><!-- carousel-caption -->
      </div><!-- item -->

      <div class="item">
        <div class="carousel-caption">
          <h3 class="carousel-section-head">The Events Include</h3>
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>
          
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>

        </div>
      </div><!-- item -->

      <div class="item active">
        <div class="carousel-caption">
          <h3 class="carousel-section-head">The Events Include</h3>
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>
          
          <div class="carousel-content col-xs-12 col-sm-6">
              <span class="glyphicon glyphicon-record" ></span>
              <h2>First slide label</h2>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </div>

        </div>
      </div><!-- item -->
    </div>
    <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>

  </div><!-- carousel-container -->


</div>  