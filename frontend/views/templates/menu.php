<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\modules\Modal;
use Zelenin\yii\SemanticUI\helpers\Size;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chat Archives';
$this->params['breadcrumbs'][] = $this->title;

/* equal heights */
$this->registerJsFile('/js/jquery.matchHeight-min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS
 $('.archive_chat_holder').matchHeight();
JS;
$this->registerJs($js, \yii\web\View::POS_READY);

$noconflict = <<<JS
  $.fn.bsModal = $.fn.modal.noConflict();
JS;
$this->registerJs($noconflict, \yii\web\View::POS_READY);
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <span class="search_btn hidden-md hidden-lg" >

        <?php $searchmodal = Modal::begin([
            'size' => Size::LARGE,
            'header' => 'Filter Archives',
            'actions' => Elements::button('Exit' . Elements::icon('remove'), ['class' => 'cancel right labeled icon'])
        ]); ?>
          <?= $this->render('@frontend/view/chat/_archivesearch', ['model' => $searchModel]);?>

        <?php $searchmodal::end(); ?>
        <?= $searchmodal->renderToggleButton(Elements::icon('search')) ?>

        </span>
        
        <div class="hidden-sm hidden-xs">
                <?php echo $this->render('@frontend/view/chat/_archivesearch', ['model' => $searchModel]); ?>
        </div>
      <ul class="nav navbar-nav navbar-right">
      
        <li><a href="<?= Url::toRoute(['/',])?>" ><span class="fa fa-home"></span>Home</a></li>
    <li ><a href="<?= Url::toRoute(['site/register#login'])?>" ><span class="fa fa-lock "></span>LOGIN</a></li>
    <li><a href=""><span class="fa fa-comment-o"></span>ENTER CHAT ROOM</a> </li>
   <li><a href="<?= Url::to(['/site/help'])?>"><span class="fa fa-question"></span>Help</a></li>
    
      </ul>
    </div>
  </div>
</nav>