<!-- Chat details
================================================== -->

<section class="chat-details-container">
   
   <div class="container"> 
    
    <div class="row">
        
        <div class="row">
        <!--  Top horizontal bar -->
            <div class="col-md-9 hidden-xs hidden-sm ">
              <span class="horizontal-rule"></span>
            </div>

            <div class="col-sm-12 col-md-3">
              <span class="bordered-btns-container">
                <button class="btn-bordered btn-grey btn-default">Button Grey </button>
                <button class="btn-bordered btn-orange btn-default"> Button orange</button>
              </span>
            </div>

            <div class="main-chat-head col-sm-12">  
                <h2>Welcome to the Chat Room</h2>
            </div>
        </div>
        
        <div class="conversation-body">
          
          <!--  current-online-user  -->
          <div class="current-online-user avatar-panel"> 
            <table class="table table-responsive">
              <tbody>
                <tr>
                    <td class="current-user-avatar col-xs-4">
                      <img class="img-circle" src="http://placehold.it/100x100" alt="...">
                    </td>
                    <td class="current-user-info col-xs-8">
                      <a href="#" class="exit-heading">EXIT CHATROOM</a>
                      <a href="#" class="edit-profile-link">Edit my personal profile</a>
                    </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--  END current-online-user  -->


          <!--  Chat Body -->
            <div class="conversation-area col-sm-12 col-md-9">
              
                  <div class="chat-body">
                    
                      <!--  per-chat  -->
                      <div class="per-chat">

                         <div class="col-xs-12 col-sm-3 col-md-2 user-name">
                            <span>Name of Person</span>
                         </div><!-- user-name -->

                          <div class="col-xs-12 col-sm-9 col-md-10 posted-chat">
                            <span class=" conversation col-sm-11 col-xs-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil aspernatur ipsa quos sunt voluptas eius, officiis mollitia expedita qui maxime alias nam nisi sed. Ipsum, ab sint earum molestias nobis.</span>
                            <span class="col-sm-1 posted-chat-actions hidden-xs">
                              <input class="conversation-checkbox" type="checkbox">
                              <a href="#" class="conversation-btn glyphicon glyphicon-refresh"></a>
                            </span><!-- media -->
                          </div><!-- posted-chat -->

                      </div>

                      <div class="per-chat">

                         <div class="col-xs-12 col-sm-3 col-md-2 user-name">
                            <span>Name of Person</span>
                         </div><!-- user-name -->

                          <div class="col-xs-12 col-sm-9 col-md-10 posted-chat">
                            <span class=" conversation col-sm-11 col-xs-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi voluptate non, odio esse expedita dolorum, ad, necessitatibus amet quia consectetur officia pariatur, sapiente qui minima accusantium similique molestias fugiat dolor.</span>
                            <span class="col-sm-1 posted-chat-actions hidden-xs">
                              <input class="conversation-checkbox" type="checkbox">
                              <a href="#" class="conversation-btn glyphicon glyphicon-refresh"></a>
                            </span><!-- media -->
                          </div><!-- posted-chat -->

                      </div>

                      <div class="per-chat">

                         <div class="col-xs-12 col-sm-3 col-md-2 user-name">
                            <span>Name of Person</span>
                         </div><!-- user-name -->

                          <div class="col-xs-12 col-sm-9 col-md-10 posted-chat">
                            <span class=" conversation col-sm-11 col-xs-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, molestias in, eveniet ut enim deserunt tempore deleniti numquam dignissimos ducimus nihil a culpa minima sit odio, optio nam omnis pariatur.</span>
                            <span class="col-sm-1 posted-chat-actions hidden-xs">
                              <input class="conversation-checkbox" type="checkbox">
                              <a href="#" class="conversation-btn glyphicon glyphicon-refresh"></a>
                            </span><!-- media -->
                          </div><!-- posted-chat -->

                      </div>

                      <div class="per-chat">

                         <div class="col-xs-12 col-sm-3 col-md-2 user-name">
                            <span>Name of Person</span>
                         </div><!-- user-name -->

                          <div class="col-xs-12 col-sm-9 col-md-10 posted-chat">
                            <span class=" conversation col-sm-11 col-xs-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos animi, porro dolore quidem aspernatur pariatur itaque ad temporibus deleniti molestias neque doloribus, architecto, ipsum ab labore. Possimus, incidunt dolorum ab.</span>
                            <span class="col-sm-1 posted-chat-actions hidden-xs">
                              <input class="conversation-checkbox" type="checkbox">
                              <a href="#" class="conversation-btn glyphicon glyphicon-refresh"></a>
                            </span><!-- media -->
                          </div><!-- posted-chat -->

                      </div>

                      <div class="per-chat">

                         <div class="col-xs-12 col-sm-3 col-md-2 user-name">
                            <span>Name of Person</span>
                         </div><!-- user-name -->

                          <div class="col-xs-12 col-sm-9 col-md-10 posted-chat">
                            <span class=" conversation col-sm-11 col-xs-12">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam harum possimus quae, deserunt voluptates quidem dicta corrupti natus error ipsa id. Provident eveniet similique vel possimus, doloribus illum molestias, excepturi.</span>
                            <span class="col-sm-1 posted-chat-actions hidden-xs">
                              <input class="conversation-checkbox" type="checkbox">
                              <a href="#" class="conversation-btn glyphicon glyphicon-refresh"></a>
                            </span><!-- media -->
                          </div><!-- posted-chat -->

                      </div>

                      <!-- ends per-chat -->
                      <!-- ====== -->
                    
                  </div><!-- chat-body -->
                
                <!-- start chat-form-->
                <span class="chat-form">
                    <div class="input-group">
                        <input type="text" class="chat-form-input form-control" placeholder="Type a message..." value="">
                        <span class=" input-group-btn">
                            <button type="button" class="chat-form-input-btn btn btn-default">
                             <span class="glyphicon glyphicon-arrow-right"></span>
                            </button>
                        </span>
                    </div>
                </span>

                  <!-- ============================== -->
            </div> <!-- ENDS CHAT AREA -->

               
          
            <div class="online-user-container col-xs-12 col-md-3">
              
                
                
                <!-- BEGIN ONLINE USER LIST-->
                <div class="online-users-list">
                 
                  <div class="online-users-intro">
                        <span id="user-count">40 </span> users online
                        <h4 class="orange-header">Online Users</h4>
                  </div>
                  
                  <!-- BEGIN ONLINE USER LIST CONTAINER-->
                  <div class="online-users-list-container">

                       <div class="avatar-panel col-xs-6 col-sm-4 col-md-12"> 
                          <table class="online-user-panel table table-bordered table-responsive table-condensed">
                            <tbody>
                              <tr data-placement="top" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true">
                                   <td class="online-user-avatar">
                                      <img class="img-circle" src="http://placehold.it/70x70" alt="...">
                                    </td>
                                    <td class="online-user-info">
                                      <span class="avatar-bio">Persons Name</span>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        

                        <div class="avatar-panel col-xs-6 col-sm-4 col-md-12"> 
                          <table class="online-user-panel table table-bordered table-responsive table-condensed">
                            <tbody>
                              <tr data-placement="top" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true">
                                   <td class="online-user-avatar">
                                      <img class="img-circle" src="http://placehold.it/70x70" alt="...">
                                    </td>
                                    <td class="online-user-info">
                                      <span class="avatar-bio">Persons Name</span>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                        <div class="avatar-panel col-xs-6 col-sm-4 col-md-12"> 
                          <table class="online-user-panel table table-bordered table-responsive table-condensed">
                            <tbody>
                              <tr data-placement="top" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true">
                                   <td class="online-user-avatar">
                                      <img class="img-circle" src="http://placehold.it/70x70" alt="...">
                                    </td>
                                    <td class="online-user-info">
                                      <span class="avatar-bio">Persons Name</span>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                        <div class="avatar-panel col-xs-6 col-sm-4 col-md-12"> 
                          <table class="online-user-panel table table-bordered table-responsive table-condensed">
                            <tbody>
                              <tr data-placement="top" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true">
                                   <td class="online-user-avatar">
                                      <img class="img-circle" src="http://placehold.it/70x70" alt="...">
                                    </td>
                                    <td class="online-user-info">
                                      <span class="avatar-bio">Persons Name</span>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                        <div class="avatar-panel col-xs-6 col-sm-4 col-md-12"> 
                          <table class="online-user-panel table table-bordered table-responsive table-condensed">
                            <tbody>
                              <tr data-placement="top" data-toggle="popover" data-container="body" data-placement="left" type="button" data-html="true">
                                   <td class="online-user-avatar">
                                      <img class="img-circle" src="http://placehold.it/70x70" alt="...">
                                    </td>
                                    <td class="online-user-info">
                                      <span class="avatar-bio">Persons Name</span>
                                    </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>


                    </div> <!-- END ONLINE USER LIST container -->

                  <!-- END ONLINE USER LIST -->
                </div> <!-- row -->

            </div>

        <!-- END conversation-body -->
        </div> <!-- conversation-body  -->
                
    </div><!-- main row -->
  </div><!-- main container -->
</section> <!-- container-section -->



<!-- Popover -->
<div class="hide" id="userDetailsModal">
   <div class="thumbnail">
     <a class="bio-close-btn glyphicon glyphicon-remove-circle" role="button" onclick="$(&quot;[data-toggle=popover]&quot;).popover(&quot;hide&quot;);"></a>
     <img alt="User Avatar" src="http://placehold.it/150x150">
     <div class="caption">
         <p>John Doe</p>
         <p>Company Name</p>
         <p>email@domain.com</p>
     </div>
   </div>
</div><!-- /.modal -->