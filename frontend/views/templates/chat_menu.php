<?php
use yii\helpers\Url;
use yii\web\View;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\helpers\Size;
use Zelenin\yii\SemanticUI\modules\Modal;
?>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <div class="container">
      <div class="navbar-header">
        <h3 class="nav-brand" style="text-transform:initial;color:#95C13D">Welcome To The KCB Group Chat Room</h3>

    </div>

     <!--  <ul class="nav navbar-nav welcome-nav "><li><h3 style="color:#95C13D; text-transform: initial;">Welcome To The Chat Room</h3></li></ul> -->
      <ul class="nav navbar-nav navbar-right" style="margin-right: -155px;">

        <li><a href="<?= Url::toRoute(['/',])?>"  style><span class="fa fa-home"></span>Home</a></li>
        <li><?= $reviewmodal->renderToggleButton('< EXIT CHAT ROOM',['class'=>'exit-chat','style'=>'color:#fff;font-weight:400;margin-top:6px; font-size:14px']);?></li>
        <li ><a href="<?= Url::to(['/site/logout'])?>" data-method="post" > < Logout</a></li>
        <li><a href="<?= Url::to(['/site/help'])?>"><span class="fa fa-question"></span>Help</a></li>
        </ul>
       </div>
    </div>
  </div>
</nav>
