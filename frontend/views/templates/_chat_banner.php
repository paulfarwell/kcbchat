<?php
use yii\helpers\Html;
$settings = Yii::$app->settings;
$settings->clearCache();
$logo = $settings->get('Theme.logo');
$banner = $settings->get('Theme.banner');

?>
<nav class="navbar navbar-inverse ">
 <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
    <div class="navbar-header">
         <div class="navbar-brand">
                  <?php echo Html::a( Html::img('@web/images/site/'. $logo, ['class' => 'slider-image']), ['/chat']); ?>
          </div>

    </div>
  </div>
  <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
 <div id="slider" class="pull-right chat-banner">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="9000">
  <!-- Indicators -->
  <!-- <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
  </ol> -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <?php echo Html::a( Html::img('@web/banner/KCB Strategic Points Banner Design_01_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
    <div class="item">
    <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_02_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
     <div class="item">
    <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_03_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
     <div class="item">
    <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_04_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
     <div class="item">
    <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_05_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
     <div class="item">
    <?php echo Html::a(Html::img('@web/banner/KCB Strategic Points Banner Design_06_v2.png', ['class' => 'slider-image']), ['/chat']); ?>
    </div>
  </div>
  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
</div>
   </div>
</nav>
