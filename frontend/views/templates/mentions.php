<?php
$this->registerJsFile('/js/jquery.slimscroll.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/js/mentions-notification-helper.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<div class="mention-wrapper">
	
	<ul id="nav" class="mention_ul">
		<li id="mention_li" class="pull-right">
		<button class="btn btn-default btn-lg btn-link pull-right" style="font-size:24px;" id="mentionLink">
		    <span class="glyphicon glyphicon-comment"></span>
		    <span class="badge badge-notify" id="mention_count"></span>
		</button>
		
	<!--
		<a href="#" id="mentionLink"><span id="mention_count">3</span><i class="nav-icon fa fa-bullhorn"></i> 
		<span class="small-screen-text">mention</span></a>-->

		<div id="mentionContainer">
			
		<!--<div id="mentionTitle">New Messages</div>-->
		<div id="mentionBody" class="mention"></div>
		<!--<div id="mentionFooter"><a href="#">See All</a></div>-->
		</div>

		</li>
	</ul>
</div>