<?php
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Countries;

/**
 * @var yii\web\View              $this
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/farwell.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php
$this->registerCssFile("/css/skins/minimal.css");
$this->registerJsFile('/js/icheck.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$jscheckbox = <<<JS
 
  $('input').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
    increaseArea: '20%' // optional
  });

JS;
$this->registerJs($jscheckbox, \yii\web\View::POS_READY);

?>

<section class="page-register chat-details-container container-grey" id="user_register">
    <div class="container section-register">

        <div class="row">

            <div class="col-md-6">
                <div class="about-chat">
                        <h1>Registration </h1>

                </div>
            </div><!-- col-md-6 -->

            <div class="col-md-6" id="register">
            
                    <div class="registration-form form-white">
                       
                                <?php $form = ActiveForm::begin([
                                    'id' => 'registration-form',
                                    'class' => 'form-horizontal',
                                    'layout' => 'horizontal',
                                    'fieldConfig' => [
                                        'horizontalCssClasses' => [
                                            'label' => 'col-xs-12 col-sm-3',
                                            'offset' => 'col-xs-offset-4',
                                            'wrapper' => 'col-xs-12 col-sm-9',
                                            'error' => '',
                                            'hint' => '',
                                        ],
                                        'horizontalCheckboxTemplate'=>"<div class=\"form-group\">\n<div class=\"checkbox\">\n<label class=\"col-sm-8 control-label\">\n{labelTitle}\n{error}\n</label>\n<div class=\"col-sm-2 col-xs-2\">\n{input}\n</div>\n</div>\n</div>\n{hint}",
                                        
                                    ],
                                ]); ?>

                                <?= $form->field($model, 'name') ?>

                                <?= $form->field($model, 'lastname') ?>

                                <?= $form->field($model, 'email') ?>

                               <!-- <?= $form->field($model, 'username') ?>-->

                                <?= $form->field($model, 'password')->passwordInput() ?>


                                <?= $form->field($model, 'confirm_password')->passwordInput() ?>

                                <?= $form->field($model, 'country')
                                        ->dropDownList(
                                            ArrayHelper::map(Countries::find()->orderBy('CountryName')->all(), 'ID', 'CountryName'),         
                                            ['prompt'=>'Select Country']   
                                        );
                                 ?>

                                <?= $form->field($model, 'city') ?>
                                
                                <?= $form->field($model, 'ageagree')->checkbox() ?>

                                 <?= $form->field($model, 'termsagree')->checkbox(); ?>
                                

                                <p class="email_note">* The system will send an email. Please click to authenticate your Chat registration</p>

                                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-default sign-up pull-right']) ?>

                                <?php ActiveForm::end(); ?>

                    </div><!-- registration-form -->
                <div class="clearfix"></div>
            <p class="pull-right">
                <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login'], ['class'=> 'make-block']) ?>
            </p>

            </div><!-- col-md-6 -->

         </div><!-- row -->


 </div> <!-- main container -->
</section> <!-- container-section -->

