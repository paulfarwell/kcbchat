<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

/*
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $model dektrium\user\models\SettingsForm
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('@frontend/views/templates/profile_menu')?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row user_profile">
 <!-- page-title -->

    <div class="container">

        <div class="row">
            <?= Breadcrumbs::widget([
             'homeLink' => [ 
                          'label' => Yii::t('yii', 'Home'),
                          'url' => Yii::$app->homeUrl,
                     ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <span class="row">

        <div class="col-md-3">
            <?= $this->render('_menu') ?>
        </div>
        <div class="col-md-9">
            <div class="profile_edit ">
                
                    <?php $form = ActiveForm::begin([
                        'id'          => 'account-form',
                        'options'     => ['class' => 'form-horizontal '],
                        'fieldConfig' => [
                            'template'     => "{label}\n<div class=\"col-sm-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-sm-9\">{error}\n{hint}</div>",
                            'labelOptions' => ['class' => 'col-sm-3 control-label'],
                        ],
                        'enableAjaxValidation'   => true,
                        'enableClientValidation' => false,
                    ]); ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'username') ?>

                    <?= $form->field($model, 'current_password')->passwordInput() ?>

                    <hr />
                    
                    <?= $form->field($model, 'new_password')->passwordInput() ?>
                    
                    <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>

                    <div class="form-group">
                        <div class="col-lg-4 pull-right">
                            <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-join', 'style'=>'color:#064367;font-weight:bold']) ?><br>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                
            </div>
        </div>
    </div><!-- container -->
</div>
