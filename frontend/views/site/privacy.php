<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<link rel="stylesheet" href="base.min.css"/>
<link rel="stylesheet" href="fancy.min.css"/>
<link rel="stylesheet" href="main.css"/>
<script src="compatibility.min.js"></script>
<script src="theViewer.min.js"></script>
<script>
try{
theViewer.defaultViewer = new theViewer.Viewer({});
}catch(e){}
</script>
<title></title>
</head>
<body>
<div id="sidebar">
<div id="outline">
</div>
</div>
<div id="page-container">
<div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="@web/images/logo.png"/><div class="t m0 x1 h2 y1 ff1 fs0 fc0 sc0 ls0 ws0"> </div><div class="t m0 x2 h2 y2 ff1 fs0 fc0 sc0 ls0 ws0"> </div><div class="t m0 x2 h3 y3 ff2 fs1 fc0 sc0 ls0 ws0">“Kcb Group Chat”<span class="ff1"> </span></div>
<div class="t m0 x2 h2 y4 ff1 fs0 fc0 sc0 ls1 ws0"> <span class="_ _0"> </span><span class="ls0"> </span></div>
<div class="t m0 x2 h4 y5 ff3 fs0 fc0 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h5 y6 ff3 fs2 fc0 sc0 ls0 ws0">Munir Sheikh Ahmed, CEO National Bank </div>
<div class="t m0 x2 h5 y7 ff3 fs2 fc0 sc0 ls0 ws0">TOPIC | Real Estate &amp; Construction </div>
<div class="t m0 x2 h5 y8 ff3 fs2 fc0 sc0 ls0 ws0">WEDNESDAY FEBRUARY 25</div>
<div class="t m0 x3 h6 y9 ff3 fs3 fc0 sc0 ls2 ws0">TH</div><div class="t m0 x4 h5 y8 ff3 fs2 fc0 sc0 ls0 ws0"> 2015 </div>
<div class="t m0 x2 h5 ya ff3 fs2 fc0 sc0 ls0 ws0">Time: 10:00am-11:00am </div>
<div class="t m0 x2 h7 yb ff4 fs0 fc1 sc0 ls0 ws0"> </div><div class="t m0 x5 h2 yc ff1 fs0 fc1 sc0 ls0 ws0"> </div><div class="t m0 x2 h2 yd ff1 fs0 fc1 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 ye ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 yf ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y10 ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y11 ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y12 ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y13 ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y14 ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y15 ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y16 ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y17 ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y18 ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y19 ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y1a ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h8 y1b ff5 fs4 fc2 sc0 ls0 ws0"> </div>
<div class="t m0 x2 h8 y1c ff5 fs4 fc2 sc0 ls0 ws0"> </div><div class="t m0 x2 h9 y1d ff6 fs2 fc1 sc0 ls0 ws0"> </div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>

</div>
</body>
</html>