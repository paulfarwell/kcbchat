<?php
namespace frontend\components;

use Yii;
use \yii\base\BootstrapInterface;

class ThemeBootstrap implements BootstrapInterface
{
    /**
    * Bootstrap method to be called during application bootstrap stage.
    * @param Application $app the application currently running
    */
    public function bootstrap($app)
    {
        // create an instance of Mobile_Detect class
        $detector = new \Mobile_Detect();
 
        if ($detector->isMobile()) {

        // modify the configuration of our view component
        $app->set('view', [
            'class' => 'yii\web\View',
            'theme' => [
                'pathMap' => [
                	'@app/views' => '@app/themes/mobile',
                    '@dektrium/user/views' => '@app/views/user',
                    '@pheme/settings/views' => '@app/views/settings',
                    '@sjaakp/illustrated/views' => '@app/views/illustrated'
                ],
            ],
        ]);
        }
    }
}