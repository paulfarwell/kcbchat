<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use common\models\Participate;
use yii\db\Query;

/**
 * ChatSearch represents the model behind the search form about `common\models\Chat`.
 */
class ParticipantSearch extends Participate
{
    /**
     * @inheritdoc
     */

public $branch;
public $country;
    public function rules()
    {
        return [
          [['ID', 'Chat', 'User'], 'integer'],
           [['branch','country'], 'safe'],
        ];
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    // public function search($params)
    // {
    //
    //     $query = Participate::find()->where(['Chat' => $params['chat']]);
    //
    //     $dataProvider = new ActiveDataProvider([
    //         'query' => $query,
    //     ]);
    //     $query->joinWith(['user','chat']);
    //      $this->load($params);
    //     //
    //     // if (!$this->validate()) {
    //     //     // uncomment the following line if you do not want to return any records when validation fails
    //     //     // $query->where('0=1');
    //     //     return $dataProvider;
    //     // }
    //     //
    //     // $query->andFilterWhere([
    //     //     'ID' => $this->ID,
    //     //     'Edition' => $this->Edition,
    //     //     'ChatDate' => $this->ChatDate,
    //     //     'Host' => $this->Host,
    //     //     'Status' => $this->Status,
    //     //     'ParticipantsLimt' => $this->ParticipantsLimt,
    //     // ]);
    //     //
    //     // $query->andFilterWhere(['like', 'StartTime', $this->StartTime])
    //     //     ->andFilterWhere(['like', 'EndTime', $this->EndTime])
    //     //     ->andFilterWhere(['like', 'Topic', $this->Topic])
    //     //     ->andFilterWhere(['like', 'Description', $this->Description])
    //     //     ->andFilterWhere(['like', 'Log', $this->Log])
    //     //     ->andFilterWhere(['like', 'Podcast', $this->Podcast]);
    //     return $dataProvider;
    // }

    public function search($params)
    {
        $query = Participate::find()->where(['Chat' => $params['chat']]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->joinWith(['user','chat','profile']);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'Chat' => $this->Chat,
            'User' => $this->User,

        ]);

         $query->andFilterWhere(['like', 'user.profile.branch', $this->branch])
               ->andFilterWhere(['like', 'user.profile.country', $this->country]);

        return $dataProvider;
    }



}
