<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use common\models\Chat;
use yii\db\Query;

/**
 * ChatSearch represents the model behind the search form about `common\models\Chat`.
 */
class ChatSearch extends Chat
{
    /**
     * @inheritdoc
     */
    public $Year;
    public $Quarter;
    public $Topic;
    public $Ceo;

    public function rules()
    {
        return [
            [['ID', 'Edition', 'Host', 'Status', 'ParticipantsLimt'], 'integer'],
            [['ChatDate', 'StartTime', 'EndTime', 'Topic', 'Description', 'Log', 'Podcast', 'Year', 'Quarter', 'Topic', 'Ceo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getYears(){
        $connection = Yii::$app->getDb();
        //get all the years
        $sql='SELECT DISTINCT YEAR(ChatDate) as year FROM chat ORDER BY year DESC';
        return $connection -> createCommand($sql) ->queryAll() ; 

     }


    public static function getTopics(){
        $connection = Yii::$app->getDb();

        $sql='SELECT DISTINCT Topic as topic FROM chat WHERE Status=:status';
        return $connection -> createCommand($sql)->bindValue(':status', Chat::CHAT_COMPLETED)->queryAll() ; 
    }

    public static function getQuarters(){
         return [
            1=>'One',
            2=>'Two',
            3=>'Three',
            4=>'Four',
        ];
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'Edition' => $this->Edition,
            'ChatDate' => $this->ChatDate,
            'Host' => $this->Host,
            'Status' => $this->Status,
            'ParticipantsLimt' => $this->ParticipantsLimt,
        ]);

        $query->andFilterWhere(['like', 'StartTime', $this->StartTime])
            ->andFilterWhere(['like', 'EndTime', $this->EndTime])
            ->andFilterWhere(['like', 'Topic', $this->Topic])
            ->andFilterWhere(['like', 'Description', $this->Description])
            ->andFilterWhere(['like', 'Log', $this->Log])
            ->andFilterWhere(['like', 'Podcast', $this->Podcast]);

        return $dataProvider;
    }


    public function searcharchives($params)
    {

        $query = (new \yii\db\Query());

        $query->select('*')
            ->from('chat')
            ->where('Status=:status', [':status' => Chat::CHAT_COMPLETED]);
           
        if (!empty($params['ChatSearch']['Year'])) {
            $query->andWhere('YEAR(ChatDate) = :chatdate', [':chatdate' => $params['ChatSearch']['Year']]);
        }
        if (!empty($params['ChatSearch']['Topic'])) {
            $query->andWhere('Topic = :topic', [':topic' => $params['ChatSearch']['Topic']]);
        }
        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                    'pageSize' => 6,
                ]
        ]);

        return $dataProvider;
    }
}
