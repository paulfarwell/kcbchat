<?php

namespace backend\models;

use Yii;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "appearance".
 *
 * @property integer $ID
 * @property string $Description
 * @property integer $GalleryID
 */
class Appearance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appearance';
    }

    public function behaviors()
    {
        return [
             'galleryBehavior' => [
                 'class' => GalleryBehavior::className(),
                 'type' => 'appearance',
                 //'owner' => 'galleryId',
                 'extension' => 'jpg',
                 'directory' => Yii::getAlias('@images') . '/site/gallery',
                 'url' => Yii::getAlias('@imagesurl') . '/site/gallery',
                 'versions' => [
                     'small' => function ($img) {
                         /** @var \Imagine\Image\ImageInterface $img */
                         return $img
                             ->copy()
                             ->thumbnail(new \Imagine\Image\Box(200, 200));
                     },
                     'medium' => function ($img) {
                         /** @var Imagine\Image\ImageInterface $img */
                         $dstSize = $img->getSize();
                         $maxWidth = 1200;
                         if ($dstSize->getWidth() > $maxWidth) {
                             $dstSize = $dstSize->widen($maxWidth);
                         }
                         return $img
                             ->copy()
                             ->resize($dstSize);
                     },
                     'large' => function ($img) {
                         /** @var Imagine\Image\ImageInterface $img */
                         $dstSize = $img->getSize();
                         $maxWidth = 3000;
                         if ($dstSize->getWidth() > $maxWidth) {
                             $dstSize = $dstSize->widen($maxWidth);
                         }
                         return $img
                             ->copy()
                             ->resize($dstSize);
                     },
                 ]
             ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Description'], 'required'],
            [['GalleryID'], 'integer'],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Description' => 'Description',
            'GalleryID' => 'Gallery ID',
        ];
    }
}
