<?php 

namespace backend\models;


use Yii;
use yii\base\Model;
use pheme\settings\models\BaseSetting;


class Theme extends BaseSetting 
{
    public $logo;
    public $banner;
    public $homePageDescription;
    public $registrationDescription;
    public $loginDescription;
    public function rules()
    {
        return [
            [['homePageDescription','registrationDescription','loginDescription'], 'string'],
            ['logo', 'image', 'extensions' => 'png, jpg, gif',
                'minWidth' => 100, 'maxWidth' => 1200,
                'minHeight' => 100, 'maxHeight' => 1000,
            ],
            ['banner', 'image', 'extensions' => 'png, jpg, gif',
                'minWidth' =>700, 'maxWidth' =>1200,
                'minHeight' => 130, 'maxHeight' => 300,
            ],
        ];
    }

    public function fields()
    {
            return ['logo', 'homePageDescription','banner','registrationDescription','loginDescription'];
    }

    public function attributeLabels()
    {
        return [
            'logo' => 'Logo',
            'homePageDescription' => 'Home Page Introduction',
            'banner' => 'Banner',
            'registrationDescription' => 'Registration Page Introduction',
            'loginDescription' => 'Login Page Introduction',
        ];
    }
}