<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Edition;

/**
 * EditionSearch represents the model behind the search form about `common\models\Edition`.
 */
class EditionSearch extends Edition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Quarter', 'Current'], 'integer'],
            [['EditionName', 'Year', 'Theme'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Edition::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'Year' => $this->Year,
            'Quarter' => $this->Quarter,
            'Current' => $this->Current,
        ]);

        $query->andFilterWhere(['like', 'EditionName', $this->EditionName])
        ->andFilterWhere(['like', 'Theme', $this->Theme]);


        return $dataProvider;
    }
}
