<?php

namespace backend\controllers;

use Yii;
use backend\models\Host;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ChatController implements the CRUD actions for Chat model.
 */
class HostController extends Controller
{

    public function actionIndex()
    {
      
        $review = Host::find()->all();
         
        return $this->render('index', ['model' => $review]);
    }

  public function actionCreate()
    {
        $model = new Host();
 
        // new record
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
                 
        return $this->render('create', ['model' => $model]);
    }

     /**
     * Edit
     * @param integer $id
     */
    public function actionEdit($id)
    {
        $model = Host::find()->where(['id' => $id])->one();
 
        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');
         
        // update record
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['index']);
        }
         
        return $this->render('update', ['model' => $model]);
    }  


    /**
     * Delete
     * @param integer $id
     */
     public function actionDelete($id)
     {
         $model = Host::findOne($id);
         
        // $id not found in database
        if($model === null)
            throw new NotFoundHttpException('The requested page does not exist.');
             
        // delete record
        $model->delete();
         
        return $this->redirect(['index']);
     } 


}
