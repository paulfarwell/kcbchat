<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\models\Profile;
use sjaakp\illustrated\Uploader;
use yii\helpers\ArrayHelper;
use common\models\Countries;

/**
 * @var yii\web\View 					$this
 * @var dektrium\user\models\User 		$user
 * @var dektrium\user\models\Profile 	$profile
 */

?>



<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>
<p>this is the view used</p>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data'],
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>
<?= $form->field($profile, 'avatar')->widget(Uploader::className(), [
        'stylefileOptions' => [
            'btnClass' => 'btn btn-success',
            'btnText' => 'Please, browse for image'
        ],
        'cropperOptions' => [
            'diagonal' => 240,
            'margin' => 22,
            'sliderPosition' => 'right',
            'sliderOptions' => [
                'animate' => 'slow'
            ]
        ],
        'deleteOptions' => [
            'label' => '<i class="fa fa-trash"></i>', // Font Awesome icon
            'title' => 'Delete image'
        ]
    ]

) ?>
<?= $form->field($profile, 'name') ?>
<?= $form->field($profile, 'lastname') ?>

<!--
<?= $form->field($profile, 'public_email') ?>
<?= $form->field($profile, 'website') ?>-->
<?= $form->field($profile, 'organization') ?>
<?= $form->field($profile, 'designation') ?>
<?= $form->field($profile, 'twitter') ?>
<?= $form->field($profile, 'location') ?>
<?= $form->field($profile, 'gravatar_email') ?>
<?= $form->field($profile, 'country')
        ->dropDownList(
            ArrayHelper::map(Countries::find()->orderBy('CountryName')->all(), 'ID', 'CountryName'),         
            ['prompt'=>'Select Country']   
        );
 ?>
<?= $form->field($profile, 'city') ?>
<?= $form->field($profile, 'bio')->textarea() ?>
<?= $form->field($profile, 'role')->dropDownList(Profile::roleAlias(),[])  ?>  


<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
