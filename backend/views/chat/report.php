<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use common\models\Chat;

/* @var $this yii\web\View */
/* @var $model common\models\Chat */

$this->title = 'Chat Participants Reports';
$this->params['breadcrumbs'][] = ['label' => 'Chats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= Html::beginForm(['/chat/report'], 'POST'); ?>
    <div class="form-group">
    <?= Html::dropDownList('chat', null, ArrayHelper::map(Chat::find()->all(),'ID','Topic'), [
         'class' => 'form-control',
         'prompt'=> 'Select Topic'
    ]) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('POST', ['class' => 'btn btn-primary']); ?>
    </div>
    <?= Html::endForm(); ?>
</div>
<?php if(!empty($dataProvider)):?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
     'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [                      // the owner name of the model
            'attribute'=>'Name',
            'label' => 'Name',
            // 'filter'=>Edition::quarterAlias(),
            'value' => 'user.profile.fullname'

        ],
        [                      // the owner name of the model
            'attribute'=>'branch',
            'label' => 'Branch',
            // 'filter'=>Edition::quarterAlias(),
            'value' => 'user.profile.branch'

        ],
        [                      // the owner name of the model
            'attribute'=>'country',
            'label' => 'Country',
            // 'filter'=>Edition::quarterAlias(),
            'value' => 'user.profile.country'

        ],
        //
        // 'Description:ntext',
        // 'Status',
        // 'Log',
        // 'Podcast',
        // 'ParticipantsLimt',

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
<?php endif;?>
