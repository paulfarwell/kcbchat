<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;
use common\models\Chat;
use frontend\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Chat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            [                      // the owner name of the model
                'attribute'=>'Edition',
                'label' => 'Edition',
                'filter'=>Edition::quarterAlias(),
                'content' => function($data){
                    return $data->edition->EditionName;
                }

            ],
            [                      // the owner name of the model
                'attribute'=>'Host',
                'label' => 'Host',
                //'filter'=>User::CeoString(),
                'content' => function($data){
                    return $data->host->CeoString;
                }

            ],
            'Topic',
            'ChatDate',
            'StartTime',
            //'EndTime',
            [                      // the owner name of the model
                'attribute'=>'Status',
                'label' => 'Status',
                'filter'=>Chat::chatStatus(),
                'content' => function($data){
                    return $data->chatStatus($data->Status);
                }

            ],
            

            //
            // 'Description:ntext',
            // 'Status',
            // 'Log',
            // 'Podcast',
            // 'ParticipantsLimt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
