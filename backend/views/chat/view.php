<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Chat */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Chats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            [                      // the owner name of the model
                'label' => 'Edition',
                'value' => $model->edition->EditionName,
            ],            
            'ChatDate',
            'StartTime',
            'EndTime',
            'Topic',
            [                      // the owner name of the model
                'label' => 'Host',
                'value' => $model->host->CeoString,
            ], 
            'Description:ntext',
            [                      // the owner name of the model
                'label' => 'Status',
                'value' => $model->chatStatus($model->Status),
            ],
            'Log',
            'Podcast',
            'ParticipantsLimt',

            
        ],
    ]) ?>

</div>
