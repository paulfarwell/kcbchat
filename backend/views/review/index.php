<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Edition;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Review Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Review Questions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

      <table border="1" class="table table-striped table-bordered">
    <tr>
        <th>Related Chat</th>
        <th>Question 1</th>
        <th>Question 2</th>
        <th>Question 3</th>
        <th>Question 4</th>
        <th>Question 5</th>
        <th>Actions</th>
    </tr>
    <?php foreach($model as $field){ ?>
    <tr>
        <td><?= $field->Chat; ?></td>
        <td><?= $field->Question_1; ?></td>
        <td><?= $field->Question_2; ?></td>
        <td><?= $field->Question_3; ?></td>
        <td><?= $field->Question_4; ?></td>
        <td><?= $field->Question_5; ?></td>
        <td><?= Html::a("Show Reviews", ['review/show', 'id' => $field->ID]); ?> | <?= Html::a("Edit", ['review/edit', 'id' => $field->ID]); ?> | <?= Html::a("Delete", ['review/delete', 'id' => $field->ID]); ?></td>
    </tr>
    <?php } ?>
</table>
</div>
