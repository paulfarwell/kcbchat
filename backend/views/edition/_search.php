<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\EditionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="edition-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'EditionName') ?>

    <?= $form->field($model, 'Theme') ?>

    <?= $form->field($model, 'Year') ?>

    <?= $form->field($model, 'Quarter') ?>

    <?= $form->field($model, 'Current') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
