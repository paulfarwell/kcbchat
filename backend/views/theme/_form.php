<?php
/**
 * @link http://phe.me
 * @copyright Copyright (c) 2014 Pheme
 * @license MIT http://opensource.org/licenses/MIT
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\file\FileInput;
$settings = Yii::$app->settings;
$settings->clearCache();
$logo= $settings->get('Theme.logo');
if (!empty($logo)) {
	$model->logo= $logo;
}
$banner= $settings->get('Theme.banner');
if (!empty($banner)) {
	$model->banner= $banner;
}
$homePageDescription= $settings->get('Theme.homePageDescription');
if (!empty($homePageDescription)) {
	$model->homePageDescription= $homePageDescription;
}
$registrationDescription= $settings->get('Theme.registrationDescription');
if (!empty($registrationDescription)) {
	$model->registrationDescription= $registrationDescription;
}
$loginDescription= $settings->get('Theme.loginDescription');
if (!empty($loginDescription)) {
	$model->loginDescription= $loginDescription;
}


/**
 * @var yii\web\View $this
 * @var pheme\settings\models\Setting $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="setting-form">
    <?php $form = ActiveForm::begin([
    'id' => 'theme-settings-form',
    'options' => [
    	'enctype' => 'multipart/form-data'
    	]
    ]); ?>

		<?= $form->field($model, 'logo')->widget(FileInput::classname(), [
		    'options'=>[
		        'options' => ['accept' => 'image/*'],
		    ],
		    'pluginOptions' => [
		        'initialPreview'=>[
		            Html::img('@imagesurl/site/' . $model->logo, ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'Logo']),
		        ],
		        'showUpload' => false,
		        'initialCaption'=>"Logo",
		        'overwriteInitial'=>true
		    ],
		]); ?>
		<?= $form->field($model, 'banner')->widget(FileInput::classname(), [
		    'options'=>[
		        'options' => ['accept' => 'image/*'],
		    ],
		    'pluginOptions' => [
		        'initialPreview'=>[
		            Html::img('@imagesurl/site/' . $model->banner, ['class'=>'file-preview-image', 'alt'=>'The Moon', 'title'=>'Banner']),
		        ],
		        'showUpload' => false,
		        'initialCaption'=>"Banner",
		        'overwriteInitial'=>true
		    ],
		]); ?>
		<?= $form->field($model, 'homePageDescription')->widget(CKEditor::className(), [ 
            'options' => ['rows' => 6],
            'preset' => 'basic' 
         ])?>
		<?= $form->field($model, 'registrationDescription')->widget(CKEditor::className(), [ 
            'options' => ['rows' => 6],
            'preset' => 'basic' 
         ])?>
         <?= $form->field($model, 'loginDescription')->widget(CKEditor::className(), [ 
            'options' => ['rows' => 6],
            'preset' => 'basic' 
         ])?>

	<div class="form-group">
		<?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'value'=>'Create', 'name'=>'submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
