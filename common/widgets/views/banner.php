<?php 
use yii\helpers\Html;
use kartik\icons\Icon;
$this->registerCssFile('/js/slick/slick.css');
$this->registerCssFile('/js/slick/slick-theme.css');
$this->registerJsFile('/js/slick/slick.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$js = <<<JS
 $('.slickalize_left').slick({
  infinite: true,
  arrows:false,
  autoplay: true,
  autoplaySpeed: 4500,
  pauseOnHover:false,
  cssEase: 'ease-out',
  useTransform: true,
  speed:3000,
  rtl: true
});
JS;

$js2 = <<<JS
 $('.slickalize_right').slick({
  infinite: true,
  arrows:false,	
  autoplay: true,
  autoplaySpeed: 4500,
  pauseOnHover:false,
  cssEase: 'ease-out',
  useTransform: true,
  speed:3000,
});
JS;
// $js = <<<JS
//  $('.slickalize').slick({
//   infinite: true,
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   autoplay: true,
//   autoplaySpeed: 1000,
//   nextArrow: '<i class="arrow-right slick-next"></i>',
//   prevArrow: '<i class="arrow-left slick-prev"></i>',
//   responsive: [
//       {
//         breakpoint: 992,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//           adaptiveHeight: true
//         }
//       },
//     ]
// });
// JS;

$this->registerJs($js, \yii\web\View::POS_READY);
$this->registerJs($js2, \yii\web\View::POS_READY);

?>
<?php $styleOption = isset($this->context->options['style'])?$this->context->options['style']:''; ?>
<div id="<?php echo $this->context->id ?>" class="banner_images_wrap clearfix container-fluid-extend <?php echo isset($bannerItemsContent['size']) ? $bannerItemsContent['size'] : null; ?>">
	<?php if (isset($bannerItemsContent['images_a']) && isset($bannerItemsContent['images_b'])):?>
		<div class="banner_images_section">
			<div class="slickalize_left banners_left" data-slick dir="rtl">
				<?php foreach ($bannerItemsContent['images_a'] as $leftImage): ?>
					<div class="leftflip">
						<?= Html::img('@web' . $leftImage , ['alt' => 'My logo']) ?>
					</div>
				<?php endforeach;?>

			</div>
			<div class="slickalize_right banners_right" data-slick >
				<?php foreach ($bannerItemsContent['images_b'] as $rightImage): ?>
					<div class="rightflip">
						<?= Html::img('@web' . $rightImage , ['alt' => 'My logo right']) ?>
					</div>
				<?php endforeach;?>
			</div>
		</div>
		<?php if(isset($bannerItemsContent['message'])):?>
			<div class="banner_centered_info d-flex h-100">
				<div class="justify-content-center align-self-center">
					<div class="content-padder">
						<div class="banner_message">
							<?= $bannerItemsContent['message'] ?>
						</div>
						<div class="banner_buttons">
							<?php if (Yii::$app->user->isGuest): ?>
								<?= Html::a(Icon::show('user-circle-o', ['class'=>'fa-2x'], Icon::FA) .' REGISTER TODAY <small class="text-muted-weight-medium"> & be part of the Inuka MSME program  </small>' , '/site/register', ['class' => 'btn btn-orange btn-banner']) ?> 
								<?= Html::a('<i class="icon-inuka-key fa-2x"></i> LOGIN <small class="text-muted-weight-medium">to the Inuka MSME program  </small>' , '/site/login', ['class' => 'btn btn-green btn-banner']) ?> 
							<?php else: ?>
								 <?= Html::a('<i class="icon-inuka-courses-icon fa-2x"></i>View <small class="text-muted-weight-medium"> Courses  </small>' , '/training-courses/courses', ['class' => 'btn btn-orange btn-banner btn-bn-view-courses']) ?>
							<?php endif;?>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>

	<?php endif;?>
</div>