<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "online".
 *
 * @property integer $User
 * @property integer $CurrentChat
 * @property string $UserName
 * @property string $LastSeen
 * @property string $FirstSeen
 * @property integer $Status
 * @property integer $Ip
 */
class Online extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'online';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['FirstSeen'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'LastSeen',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User', 'CurrentChat', 'UserName', 'Ip'], 'required'],
            [['User', 'CurrentChat', 'Status', 'Ip'], 'integer'],
            [['LastSeen', 'FirstSeen'], 'safe'],
            [['UserName'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'User' => 'User',
            'CurrentChat' => 'Current Chat',
            'UserName' => 'User Name',
            'LastSeen' => 'Last Seen',
            'FirstSeen' => 'First Seen',
            'Status' => 'Status',
            'Ip' => 'Ip',
        ];
    }
}
