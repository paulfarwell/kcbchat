<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "edition".
 *
 * @property integer $ID
 * @property string $EditionName
 * @property string $Year
 * @property integer $Quarter
 * @property integer $Current
 *
 * @property Chat[] $chats
 */
class Edition extends \yii\db\ActiveRecord
{
    
    // the following constants are used to define the current and non current editions of the chat
    const EDITION_NOTCURRENT = 0;
    const EDITION_CURRENT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'edition';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EditionName', 'Year', 'Quarter'], 'required'],
            [['Year'], 'safe'],
            [['Quarter', 'Current'], 'integer'],
            [['EditionName','Theme'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'EditionName' => 'Edition Name',
            'Year' => 'Year',
            'Quarter' => 'Quarter',
            'Current' => 'Current',
            'Theme' => 'Theme',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChats()
    {
        return $this->hasMany(Chat::className(), ['Edition' => 'ID']);
    }

    public static function quarterAlias($code=null)
    {
        $quarters=array(
                '1' => 'ONE',
                '2'=> 'TWO',
                '3'=> 'THREE',
                '4'=> 'FOUR',
            );
        if (isset($code)) {
            return $quarters[$code];
        }
        return $quarters;

    }
    public static function currentAlias($code=null)
    {
        $current=array(
                self::EDITION_CURRENT => 'YES',
                self::EDITION_NOTCURRENT => 'NO',
            );
        if (isset($code)) {
            return $current[$code];
        }
        return $current;
    }
}
