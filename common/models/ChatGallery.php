<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $ID
 * @property string $CountryCode
 * @property string $CountryName
 */
class ChatGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['image1', 'image2','image3','image4','image5','image6'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */


    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['ID' => 'chat_id']);
    }
}
