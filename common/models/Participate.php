<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use backend\models\User;
use common\models\Chat;

use backend\models\Profile;

/**
 * This is the model class for table "participate".
 *
 * @property integer $ID
 * @property integer $User
 * @property integer $Chat
 * @property integer $Termsagree
 * @property string $RequestDate
 * @property integer $Status
*  @property string $branch
* @property string $branch
 */
class Participate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participate';
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['RequestDate'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['User', 'Chat'], 'required'],
            [['User', 'Chat', 'Termsagree', 'Status'], 'integer'],
            [['RequestDate','branch','country'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'User' => 'User',
            'Chat' => 'Chat',
            'Termsagree' => 'Termsagree',
            'RequestDate' => 'Request Date',
            'Status' => 'Status',
            'branch'=>'branch',
            'country'=>'country',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'User']);
    }
   public function getChat()
  {
      return $this->hasMany(Chat::className(), ['ID' => 'Chat']);
  }

   public function getProfile()
   {
       return $this->hasMany(Profile::className(), ['user_id' => 'User']);
   }
}
